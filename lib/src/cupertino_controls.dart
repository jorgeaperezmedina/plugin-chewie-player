import 'dart:async';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/cupertino_progress_bar.dart';
import 'package:chewie/src/material_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:open_iconic_flutter/open_iconic_flutter.dart';
import 'package:video_player/video_player.dart';

class CupertinoControls extends StatefulWidget {
  const CupertinoControls({
    @required this.backgroundColor,
    @required this.iconColor,
  });

  final Color backgroundColor;
  final Color iconColor;

  @override
  State<StatefulWidget> createState() {
    return _CupertinoControlsState();
  }
}

class _CupertinoControlsState extends State<CupertinoControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  bool _hideBack = true;
  bool _hideFoward = true;
  Timer _hideTimer;
  Timer _backOption;
  Timer _fowardOption;
  final marginSize = 5.0;
  Timer _expandCollapseTimer;
  Timer _initTimer;
  bool _dragging = false;
  VideoPlayerController controller;
  ChewieController chewieController;
  bool fullScreen = false;
  bool resetValue = false;

  String textLive = "En vivo";
  final barHeight = 48.0;

  @override
  Widget build(BuildContext context) {
    chewieController = ChewieController.of(context);
    final backgroundColor = widget.backgroundColor;
    final iconColor = widget.iconColor;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;
    final orientation = MediaQuery.of(context).orientation;

    // final barHeight = orientation == Orientation.portrait ? 30.0 : 48.0;
    final buttonPadding = orientation == Orientation.portrait ? 16.0 : 24.0;

    if (_latestValue.hasError ||
        resetValue && chewieController.isLive == false ||
        _latestValue.hasError && !_latestValue.isPlaying) {
      return Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
        child: Column(
          children: <Widget>[
            chewieController.isLive == false
                ? Expanded(
                    flex: 0,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 12.0),
                            child: _buildCloseButtonError(backgroundColor,
                                iconColor, barHeight, buttonPadding, context),
                          )
                        ],
                      ),
                    ))
                : Container(),
            Expanded(child: _resetVideo())
          ],
        ),
      );
      // : Center(
      //     child: Icon(
      //       Icons.error,
      //       color: Colors.white,
      //       size: 42,
      //     ),
      //   );
    }

    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
        // _backOption?.cancel();
        // _fowardOption?.cancel();
      },
      child: GestureDetector(
        onTap: () {
          if (_hideStuff == false) {
            setState(() {
              _hideStuff = true;
            });
          } else if (_hideStuff) {
            setState(() {
              _hideStuff = false;
            });
          }
        },
        child: Container(
          decoration: BoxDecoration(
              color: _hideStuff
                  ? Colors.transparent
                  : Colors.black.withOpacity(0.5)),
          child: GestureDetector(
            onTap: () {
              // _backOption?.cancel();
              // _fowardOption?.cancel();
              _cancelAndRestartTimer();
            },
            child: AbsorbPointer(
              absorbing: _hideStuff,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 10.0, left: 10),
                    child: chewieController.isLive
                        ? inliveTopBar()
                        : _buildRowTittle(backgroundColor, iconColor, barHeight,
                            buttonPadding),
                  ),
                  // _buildTopBar(
                  //     backgroundColor, iconColor, barHeight, buttonPadding),
                  // _buildHitArea(),
                  _buildItemCenter(iconColor, barHeight),

                  _buildBottomBar(backgroundColor, iconColor, barHeight),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _resetVideo() {
    return GestureDetector(
      onTap: () {},
      child: AnimatedOpacity(
        opacity: 1.0,
        duration: Duration(milliseconds: 300),
        child: Center(
          child: Container(
            // height: barHeight,
            // margin: EdgeInsets.all(3.0),
            // padding: EdgeInsets.only(
            //   left: 8.0,
            //   right: 8.0,
            // ),
            child: chewieController.resetVideo,
            // child: Center(
            //   child: Icon(
            //     chewieController.isFullScreen
            //         ? Icons.fullscreen_exit
            //         : Icons.fullscreen,
            //   ),
            // ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    controller.removeListener(_updateState);
    _hideTimer?.cancel();
    _backOption?.cancel();
    _fowardOption?.cancel();
    _expandCollapseTimer?.cancel();
    _initTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildItemCenter(Color iconColor, double barHeight) {
    // print(_latestValue.isBuffering);
    return validateTypevideo(iconColor, barHeight);
    // return _latestValue != null &&
    //             !_latestValue.isPlaying &&
    //             _latestValue.duration == null ||
    //         _latestValue.isBuffering
    //     ? const Expanded(
    //         child: const Center(
    //           child: const CircularProgressIndicator(),
    //         ),
    //       )
    //     : _buildHitArea(controller, iconColor, barHeight);
  }

  Widget validateTypevideo(Color iconColor, double barHeight) {
    if (chewieController.isLive) {
      return _latestValue != null && !_latestValue.isPlaying
          ? const Expanded(
              child: const Center(
                child: const CircularProgressIndicator(),
              ),
            )
          : _buildHitArea(controller, iconColor, barHeight);
    } else {
      return _buildHitArea(controller, iconColor, barHeight);
      // return _latestValue != null &&
      //             !_latestValue.isPlaying &&
      //             _latestValue.duration == null ||
      //         _latestValue.isBuffering
      //     ? const Expanded(
      //         child: const Center(
      //           child: const CircularProgressIndicator(),
      //         ),
      //       )
      //     : _buildHitArea(controller, iconColor, barHeight);
    }
  }

  Widget _buildRowTittle(Color backgroundColor, Color iconColor,
      double barHeight, double buttonPadding) {
    return Row(
      children: <Widget>[
        chewieController.closeButton
            ? _buildCloseButton(
                backgroundColor, iconColor, barHeight, buttonPadding, context)
            : Container(),
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.only(top: 12, right: 15),
            child: _buildtitleVideo(context),
          ),
        ),
      ],
    );
  }

  AnimatedOpacity _buildtitleVideo(BuildContext context) {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 0.9,
      duration: Duration(milliseconds: 300),
      child: Align(
          alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 2),
            decoration: BoxDecoration(
                // color: Colors.black45
                ),
            child: Text(
              chewieController.titleVideo,
              //   child: chewieController.isLive ? Text('') : Text("Ep."+chewieController.numberEpisode.toString()+
              // " " + chewieController.titleVideo,
              style: TextStyle(
                // decoration: TextDecoration.underline,
                color: Colors.white,
                fontSize: 17,
                // shadows: [
                //   Shadow(
                //       color: Colors.black,
                //       blurRadius: 10.0,
                //       offset: Offset(5.0, 5.0)),
                //   Shadow(
                //       color: Colors.black,
                //       blurRadius: 10.0,
                //       offset: Offset(5.0, 5.0))
                // ],
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          )),
    );
  }

  AnimatedOpacity _buildBottomBar(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
  ) {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: Container(
        color: Colors.transparent,
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.all(marginSize),
        child: Container(
          height: barHeight,
          // color: backgroundColor,
          child: Row(
            children: <Widget>[
              chewieController.isLive
                  ? Expanded(child: const Text(''))
                  : _buildPosition(iconColor),
              chewieController.isLive ? const SizedBox() : _buildProgressBar(),
              chewieController.isLive
                  ? Expanded(child: const Text(''))
                  : _buildRemaining(iconColor),
              chewieController.isLive == false
                  ? _settingQuality(barHeight)
                  : Container(),
              chewieController.allowFullScreen &&
                      chewieController.isLive == false
                  ? _buildExpandButton(barHeight)
                  : Container(),
              chewieController.allowFullScreen && chewieController.isLive
                  ? rowLiveStream(barHeight)
                  : Container(),
            ],
          ),
          // child: chewieController.isLive
          //     ? Row(
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: <Widget>[
          //           _buildPlayPause(controller, iconColor, barHeight),
          //           _buildLive(iconColor),
          //         ],
          //       )
          //     : Row(
          //         children: <Widget>[
          //           _buildSkipBack(iconColor, barHeight),
          //           _buildPlayPause(controller, iconColor, barHeight),
          //           _buildSkipForward(iconColor, barHeight),
          //           _buildPosition(iconColor),
          //           _buildProgressBar(),
          //           _buildRemaining(iconColor)
          //         ],
          //       ),
        ),
      ),
    );
  }

  Widget inliveTopBar() {
    return Container(child: _buildtitleLive()
        // child: Text("En vivo", style: TextStyle(color: Colors.white)),
        );
  }

  AnimatedOpacity _buildtitleLive() {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: Align(
          alignment: Alignment.topLeft,
          // child: Container(
          //   color: Colors.black,
          //   child: Container(
          //       margin: EdgeInsets.all(3),
          //       child: Text(
          //         chewieController.qualityType,
          //         style: TextStyle(
          //             color: Colors.white,
          //             fontSize: 20,
          //             fontWeight: FontWeight.bold),
          //       )),
          // ),
          child: Row(
            children: <Widget>[
              Container(
                color: Colors.red,
                child: Container(
                    margin: EdgeInsets.all(3),
                    child: Text(
                      "En vivo",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    )),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  color: Colors.black,
                  child: Container(
                      margin: EdgeInsets.all(4.5),
                      child: Text(
                        chewieController.qualityType,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      )))
            ],
          )),
    );
  }

  Widget rowLiveStream(double barHeight) {
    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        // Container(
        //   decoration: BoxDecoration(color: Colors.red, shape: BoxShape.circle),
        //   // margin: EdgeInsets.all(3),
        //   // height: barHeight,
        //   // child: Text("."),
        //   child: Icon(
        //     Icons.radio_button_checked,
        //     color: Colors.transparent,
        //     size: barHeight / 5,
        //   ),
        // ),
        // Container(
        //   margin: EdgeInsets.all(5.0),
        //   child: Text(
        //     'En vivo',
        //     style: TextStyle(
        //         color: Colors.white,
        //         fontWeight: FontWeight.bold,
        //         // shadows: [
        //         //   Shadow(
        //         //       color: Colors.black,
        //         //       blurRadius: 10.0,
        //         //       offset: Offset(5.0, 5.0))
        //         // ],
        //         fontSize: 17),
        //   ),
        // ),
        _settingQuality(barHeight),
        _buildExpandButton(barHeight),
      ],
    );

    // Row(
    //   // mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //   children: <Widget>[
    //     Container(
    //       decoration: BoxDecoration(color: Colors.red, shape: BoxShape.circle),
    //       // margin: EdgeInsets.all(3),
    //       // height: barHeight,
    //       // child: Text("."),
    //       child: Icon(
    //         Icons.radio_button_checked,
    //         color: Colors.transparent,
    //         size: barHeight / 5,
    //       ),
    //     ),
    //     Container(
    //       margin: EdgeInsets.all(12.0),
    //       child: Text(
    //         'En vivo',
    //         style: TextStyle(
    //             color: Colors.white,
    //             fontWeight: FontWeight.bold,
    //             // shadows: [
    //             //   Shadow(
    //             //       color: Colors.black,
    //             //       blurRadius: 10.0,
    //             //       offset: Offset(5.0, 5.0))
    //             // ],
    //             fontSize: 17),
    //       ),
    //     ),
    //     _settingQuality(barHeight),
    //     _buildExpandButton(barHeight),
    //   ],
    // );
  }

  Widget _settingQuality(double barHeight) {
    return GestureDetector(
      onTap: () {},
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          margin: EdgeInsets.only(right: 12.0),
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: _latestValue == null ||
                  _latestValue.position == null ||
                  _latestValue.duration == null
              ? Container()
              : chewieController.settingQuality,
          // child: Center(
          //   child: Icon(
          //     chewieController.isFullScreen
          //         ? Icons.fullscreen_exit
          //         : Icons.fullscreen,
          //   ),
          // ),
        ),
      ),
    );
  }

  Widget _buildLive(Color iconColor) {
    return Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: Text(
        'LIVE',
        style: TextStyle(color: iconColor, fontSize: 12.0),
      ),
    );
  }

  GestureDetector _buildExpandButton(double barHeight) {
    return GestureDetector(
      onTap: changeorientarion,
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
            height: barHeight,
            margin: EdgeInsets.only(right: 12.0),
            padding: EdgeInsets.only(
              left: 8.0,
              right: 8.0,
            ),
            child: Center(
              child: Icon(
                MediaQuery.of(context).orientation == Orientation.landscape
                    ? Icons.fullscreen_exit
                    : Icons.fullscreen,
                color: Colors.white,
                size: 30,
              ),
            )),
      ),
    );
  }

  void changeorientarion() {
    if (fullScreen == false) {
      setState(() {
        fullScreen = true;
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);
      });
    } else if (fullScreen) {
      setState(() {
        fullScreen = false;
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      });
    }
  }

  // GestureDetector _buildExpandButton(
  //   Color backgroundColor,
  //   Color iconColor,
  //   double barHeight,
  //   double buttonPadding,
  // ) {
  //   return GestureDetector(
  //     onTap: _onExpandCollapse,
  //     child: AnimatedOpacity(
  //       opacity: _hideStuff ? 0.0 : 1.0,
  //       duration: Duration(milliseconds: 300),
  //       child: ClipRRect(
  //         borderRadius: BorderRadius.circular(10.0),
  //         child: BackdropFilter(
  //           filter: ui.ImageFilter.blur(sigmaX: 10.0),
  //           child: Container(
  //             height: barHeight,
  //             padding: EdgeInsets.only(
  //               left: buttonPadding,
  //               right: buttonPadding,
  //             ),
  //             color: backgroundColor,
  //             child: Center(
  //               child: Icon(
  //                 chewieController.isFullScreen
  //                     ? OpenIconicIcons.fullscreenExit
  //                     : OpenIconicIcons.fullscreenEnter,
  //                 color: iconColor,
  //                 size: 12.0,
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  Expanded _buildHitArea(
      VideoPlayerController controller, Color iconColor, double barHeight) {
    return Expanded(
      child: GestureDetector(
          onTap: () {
            // if (_hideStuff) {
            //   setState(() {
            //     _hideStuff = false;
            //   });
            // } else if (_hideStuff == false) {
            //   setState(() {
            //     _hideStuff = true;
            //   });
            // }
            // if (_latestValue != null && _latestValue.isPlaying) {
            //   if (_displayTapped) {
            //     setState(() {
            //       _hideStuff = true;
            //     });
            //   } else
            //     _cancelAndRestartTimer();
            // } else {
            //   _playPause();

            //   setState(() {
            //     _hideStuff = true;
            //   });
            // }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              screenAlertTimer("back"),
              chewieController.isLive == false
                  ? GestureDetector(
                      onTap: () {
                        _hideBack = false;
                        controller.pause();
                        _backOption = Timer(const Duration(seconds: 1), () {
                          setState(() {
                            _hideBack = true;
                            controller.play();
                            // _backOption?.cancel();
                          });
                        });
                        _skipBack(10);
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: AnimatedOpacity(
                          opacity: _hideStuff == false ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 300),
                          child: Icon(
                            Icons.replay_10,
                            color: Colors.white,
                            size: barHeight,
                          ),
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                width: barHeight,
                child: Container(),
              ),
              Container(
                child: AnimatedOpacity(
                  opacity: _hideStuff == false ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 300),
                  child: chewieController.isLive
                      ? Container()
                      : _buildPlayPause(controller, iconColor, barHeight),
                ),
              ),
              SizedBox(
                width: barHeight,
                child: Container(),
              ),
              chewieController.isLive == false
                  ? GestureDetector(
                      onTap: () {
                        _hideFoward = false;
                        controller.pause();
                        _fowardOption = Timer(const Duration(seconds: 1), () {
                          setState(() {
                            _hideFoward = true;
                            controller.play();

                            // _fowardOption?.cancel();
                          });
                        });
                        _skipForward(10);
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: AnimatedOpacity(
                          opacity: _hideStuff == false ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 300),
                          child: Icon(
                            Icons.forward_10,
                            color: Colors.white,
                            size: barHeight,
                          ),
                        ),
                      ),
                    )
                  : Container(),
              screenAlertTimer("foward")
            ],
          )
          // child: Container(
          //   color: Colors.transparent,
          //   child: Center(
          //     child: AnimatedOpacity(
          //       opacity:
          //           _latestValue != null && !_latestValue.isPlaying && !_dragging
          //               ? 1.0
          //               : 0.0,
          //       duration: Duration(milliseconds: 300),
          //       child: GestureDetector(
          //         child: Container(
          //           decoration: BoxDecoration(
          //             color: Theme.of(context).dialogBackgroundColor,
          //             borderRadius: BorderRadius.circular(48.0),
          //           ),
          //           child: Padding(
          //             padding: EdgeInsets.all(12.0),
          //             child: Icon(Icons.play_arrow, size: 32.0),
          //           ),
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          ),
    );
    // return Expanded(
    //   child: GestureDetector(
    //     onTap: _latestValue != null && _latestValue.isPlaying
    //         ? _cancelAndRestartTimer
    //         : () {
    //             _hideTimer?.cancel();

    //             setState(() {
    //               _hideStuff = false;
    //             });
    //           },
    //     child: Container(
    //       color: Colors.transparent,
    //     ),
    //   ),
    // );
  }

  Widget screenAlertTimer(String type) {
    if (type == "back") {
      return chewieController.isLive
          ? Container()
          : Container(
              child: AnimatedOpacity(
                opacity: _hideBack == true || _hideBack == null ? 0.0 : 1.0,
                duration: Duration(milliseconds: 300),
                child: Icon(
                  Icons.timer_10,
                  color: Colors.white,
                  size: barHeight / 2,
                ),
              ),
            );
    } else {
      return chewieController.isLive
          ? Container()
          : Container(
              child: AnimatedOpacity(
                opacity: _hideFoward == true || _hideFoward == null ? 0.0 : 1.0,
                duration: Duration(milliseconds: 300),
                child: Icon(
                  Icons.timer_10,
                  color: Colors.white,
                  size: barHeight / 2,
                ),
              ),
            );
    }
  }

  GestureDetector _buildMuteButton(
    VideoPlayerController controller,
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
  ) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if (_latestValue.volume == 0) {
          controller.setVolume(_latestVolume ?? 0.5);
        } else {
          _latestVolume = controller.value.volume;
          controller.setVolume(0.0);
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 10.0),
            child: Container(
              color: backgroundColor,
              child: Container(
                height: barHeight,
                padding: EdgeInsets.only(
                  left: buttonPadding,
                  right: buttonPadding,
                ),
                child: Icon(
                  (_latestValue != null && _latestValue.volume > 0)
                      ? Icons.volume_up
                      : Icons.volume_off,
                  color: iconColor,
                  size: 20.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildPlayPause(
    VideoPlayerController controller,
    Color iconColor,
    double barHeight,
  ) {
    return _latestValue != null &&
                !_latestValue.isPlaying &&
                _latestValue.duration == null ||
            _latestValue.isBuffering ||
            _hideBack == false ||
            _hideFoward == false ||
            _dragging
        ? GestureDetector(
            child: const Center(
              child: const CircularProgressIndicator(),
            ),
          )
        : GestureDetector(
            onTap: _playPause,
            child: Container(
              height: barHeight,
              color: Colors.transparent,
              padding: EdgeInsets.only(
                left: 6.0,
                right: 6.0,
              ),
              child: Icon(
                controller.value.isPlaying
                    ? OpenIconicIcons.mediaPause
                    : OpenIconicIcons.mediaPlay,
                color: iconColor,
                size: barHeight,
              ),
            ),
          );
  }

  Widget _buildPosition(Color iconColor) {
    final position =
        _latestValue != null ? _latestValue.position : Duration(seconds: 0);

    return Padding(
      padding: EdgeInsets.only(right: 12.0, left: 12.0),
      child: _latestValue == null ||
              _latestValue.position == null ||
              _latestValue.duration == null
          ? Text(
              "",
              style: TextStyle(
                color: iconColor,
                fontSize: 15.0,
              ),
            )
          : Text(
              formatDuration(position),
              style: TextStyle(
                color: iconColor,
                fontSize: 15.0,
              ),
            ),
    );
  }

  Widget _buildRemaining(Color iconColor) {
    final position = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration - _latestValue.position
        : Duration(seconds: 0);

    return Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: _latestValue == null ||
              _latestValue.position == null ||
              _latestValue.duration == null
          ? Text(
              '',
              style: TextStyle(color: iconColor, fontSize: 15.0),
            )
          : Text(
              '-${formatDuration(position)}',
              style: TextStyle(color: iconColor, fontSize: 15.0),
            ),
    );
  }

  GestureDetector _buildSkipBack(Color iconColor, double barHeight) {
    return GestureDetector(
      // onTap: _skipBack,
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        margin: EdgeInsets.only(left: 10.0),
        padding: EdgeInsets.only(
          left: 6.0,
          right: 6.0,
        ),
        child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.skewY(0.0)
            ..rotateX(math.pi)
            ..rotateZ(math.pi),
          child: Icon(
            OpenIconicIcons.reload,
            color: iconColor,
            size: 12.0,
          ),
        ),
      ),
    );
  }

  GestureDetector _buildSkipForward(Color iconColor, double barHeight) {
    return GestureDetector(
      // onTap: _skipForward,
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 6.0,
          right: 8.0,
        ),
        margin: EdgeInsets.only(
          right: 8.0,
        ),
        child: Icon(
          OpenIconicIcons.reload,
          color: iconColor,
          size: 12.0,
        ),
      ),
    );
  }

  Widget _buildTopBar(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
  ) {
    return Container(
      height: barHeight,
      margin: EdgeInsets.only(
        top: marginSize,
        right: marginSize,
        left: marginSize,
      ),
      child: Row(
        children: <Widget>[
          // chewieController.allowFullScreen
          //     ? _buildExpandButton(
          //         backgroundColor, iconColor, barHeight, buttonPadding)
          //     : Container(),
          chewieController.closeButton && !chewieController.allowFullScreen
              ? _buildCloseButton(
                  backgroundColor, iconColor, barHeight, buttonPadding, context)
              : Container(),
          Expanded(child: Container()),
          chewieController.allowMuting
              ? _buildMuteButton(controller, backgroundColor, iconColor,
                  barHeight, buttonPadding)
              : Container(),
        ],
      ),
    );
  }

  GestureDetector _buildCloseButtonError(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
    BuildContext context,
  ) {
    return GestureDetector(
      onTap: () {
        // _backOption?.cancel();
        // _fowardOption?.cancel();

        controller.pause();
        Navigator.pop(context);
      },
      child: AnimatedOpacity(
        opacity: 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.grey[700].withOpacity(0.6),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            // color: Colors.grey[700].withOpacity(0.6),
            height: barHeight,
            child: Center(
              child: Icon(
                Icons.cancel,
                color: iconColor,
                size: barHeight,
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildCloseButton(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
    BuildContext context,
  ) {
    return GestureDetector(
      onTap: () {
        // _backOption?.cancel();
        // _fowardOption?.cancel();
        _dispose();
        controller.pause();
        Navigator.pop(context);
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.grey[700].withOpacity(0.6),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            // color: Colors.grey[700].withOpacity(0.6),
            height: barHeight,
            child: Center(
              child: Icon(
                Icons.cancel,
                color: iconColor,
                size: barHeight,
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();

    setState(() {
      _hideStuff = false;

      _startHideTimer();
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController.autoPlay) {
      _startHideTimer();
    }

    if (chewieController.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController.toggleFullScreen();
      _expandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(right: 12.0),
        child: MaterialVideoProgressBar(
          controller,
          onDragStart: () {
            if (_latestValue != null ||
                _latestValue.position != null ||
                _latestValue.duration != null) {
              setState(() {
                _dragging = true;
              });

              _hideTimer?.cancel();
            }
          },
          onDragUpdate: () {
            if (_latestValue != null ||
                _latestValue.position != null ||
                _latestValue.duration != null) {
              setState(() {
                controller.pause();
                _dragging = true;
              });

              _hideTimer?.cancel();
            }
          },
          onDragEnd: () {
            if (_latestValue != null ||
                _latestValue.position != null ||
                _latestValue.duration != null) {
              setState(() {
                _dragging = false;
              });

              _startHideTimer();
            }
          },
          colors: chewieController.materialProgressColors ??
              ChewieProgressColors(
                  playedColor: chewieController.playedColor == null
                      ? Theme.of(context).accentColor
                      : Color(chewieController.playedColor),
                  handleColor: chewieController.playedColor == null
                      ? Theme.of(context).accentColor
                      : Color(chewieController.handleColor),
                  bufferedColor: chewieController.playedColor == null
                      ? Theme.of(context).backgroundColor
                      : Color(chewieController.bufferedColor),
                  backgroundColor: chewieController.playedColor == null
                      ? Colors.grey[700].withOpacity(0.7)
                      : Color(chewieController.backgroundColor)),
        ),
        // child: CupertinoVideoProgressBar(
        //   controller,
        //   onDragStart: () {
        //     if (_latestValue != null ||
        //         _latestValue.position != null ||
        //         _latestValue.duration != null) {
        //       // setState(() {
        //       //   _dragging = true;
        //       // });
        //       _hideTimer?.cancel();
        //     }
        //   },
        //   onDragEnd: () {
        //     if (_latestValue != null ||
        //         _latestValue.position != null ||
        //         _latestValue.duration != null) {
        //       // setState(() {
        //       //   _dragging = false;
        //       // });
        //       _startHideTimer();
        //     }
        //   },
        //   colors: chewieController.cupertinoProgressColors ??
        //       ChewieProgressColors(
        //         playedColor: chewieController.playedColor == null
        //             ? Theme.of(context).accentColor
        //             : Color(chewieController.playedColor),
        //         handleColor: chewieController.playedColor == null
        //             ? Theme.of(context).accentColor
        //             : Color(chewieController.handleColor),
        //         bufferedColor: chewieController.playedColor == null
        //             ? Theme.of(context).backgroundColor
        //             : Color(chewieController.bufferedColor),
        //         backgroundColor: chewieController.playedColor == null
        //             ? Colors.grey[700].withOpacity(0.7)
        //             : Color(chewieController.backgroundColor),
        //       ),
        // ),
      ),
    );
  }

  void _playPause() {
    bool isFinished = _latestValue.position >= _latestValue.duration;

    setState(() {
      if (controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        controller.pause();
      } else {
        _cancelAndRestartTimer();

        if (!controller.value.initialized) {
          controller.initialize().then((_) {
            controller.play();
          });
        } else {
          if (isFinished) {
            controller.seekTo(Duration(seconds: 0));
          }
          controller.play();
        }
      }
    });
  }

  void _skipBack(int numberTime) {
    _cancelAndRestartTimer();
    final beginning = Duration(seconds: 0).inMilliseconds;
    final skip =
        (_latestValue.position - Duration(seconds: numberTime)).inMilliseconds;
    controller.seekTo(Duration(milliseconds: math.max(skip, beginning)));
  }

  void _skipForward(int numberTime) {
    _cancelAndRestartTimer();
    final end = _latestValue.duration.inMilliseconds;
    final skip =
        (_latestValue.position + Duration(seconds: numberTime)).inMilliseconds;
    controller.seekTo(Duration(milliseconds: math.min(skip, end)));
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    if (!mounted) {
      // setState(() {});
    } else {
      setState(() {
        _latestValue = controller.value;
        if (_latestValue.duration != null) {
          resetValue = _latestValue.position >= _latestValue.duration;
        }
      });
    }

    // setState(() {});
  }
}
