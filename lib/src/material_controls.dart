import 'dart:async';
import 'dart:math' as math;

import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/material_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class MaterialControls extends StatefulWidget {
  const MaterialControls({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MaterialControlsState();
  }
}

class _MaterialControlsState extends State<MaterialControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  bool _hideBack = true;
  bool _hideFoward = true;
  Timer _hideTimer;
  Timer _backOption;
  Timer _fowardOption;
  Timer _initTimer;
  Timer _showAfterExpandCollapseTimer;
  bool _dragging = false;
  bool _displayTapped = false;
  bool fullScreen = false;
  bool resetValue = false;
  int skipNumberAction = 10;
  int skipNumberActionF = 10;

  final barHeight = 48.0;
  final marginSize = 5.0;

  VideoPlayerController controller;
  ChewieController chewieController;

  @override
  Widget build(BuildContext context) {
    if (_latestValue.hasError || resetValue) {
      return Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
        child: _resetVideo(),
      );
      // : Center(
      //     child: Icon(
      //       Icons.error,
      //       color: Colors.white,
      //       size: 42,
      //     ),
      //   );
    }

    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
      },
      child: GestureDetector(
        onTap: () {
          if (_hideStuff == false) {
            setState(() {
              _hideStuff = true;
            });
          } else if (_hideStuff) {
            setState(() {
              _hideStuff = false;
            });
          }
        },
        child: Container(
          decoration: BoxDecoration(
              color: _hideStuff
                  ? Colors.transparent
                  : Colors.black.withOpacity(0.5)),
          child: GestureDetector(
            onTap: () {
              _cancelAndRestartTimer();
            },
            child: AbsorbPointer(
              absorbing: _hideStuff,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 10.0, left: 10),
                    child: chewieController.isLive
                        ? inliveTopBar()
                        : _buildRowTittle(),
                  ),
                  // _latestValue != null &&
                  //             !_latestValue.isPlaying &&
                  //             _latestValue.duration == null ||
                  //         _latestValue.isBuffering
                  //     ? const Expanded(
                  //         child: const Center(
                  //           child: const CircularProgressIndicator(),
                  //         ),
                  //       )
                  //     : _buildItemCenter(),
                  // : _buildHitArea(),
                  _buildItemCenter(),
                  _buildBottomBar(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _resetVideo() {
    return GestureDetector(
      onTap: () {},
      child: AnimatedOpacity(
        opacity: 1.0,
        duration: Duration(milliseconds: 300),
        child: Center(
          child: Container(
            // height: barHeight,
            // margin: EdgeInsets.all(3.0),
            // padding: EdgeInsets.only(
            //   left: 8.0,
            //   right: 8.0,
            // ),
            child: chewieController.resetVideo,
            // child: Center(
            //   child: Icon(
            //     chewieController.isFullScreen
            //         ? Icons.fullscreen_exit
            //         : Icons.fullscreen,
            //   ),
            // ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    controller.removeListener(_updateState);
    _hideTimer?.cancel();
    _backOption?.cancel();
    _fowardOption?.cancel();
    _initTimer?.cancel();
    _showAfterExpandCollapseTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildItemCenter() {
    return chewieController.isLive
        ? _latestValue != null &&
                    !_latestValue.isPlaying &&
                    _latestValue.duration == null ||
                _latestValue.isBuffering
            ? const Expanded(
                child: const Center(
                  child: const CircularProgressIndicator(),
                ),
              )
            : _buildHitArea()
        : _buildHitArea();
  }

  Widget _buildRowTittle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.only(left: 15, top: 12, right: 15),
            child: _buildtitleVideo(context),
          ),
        ),
      ],
    );
  }

  AnimatedOpacity _buildtitleVideo(BuildContext context) {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 0.9,
      duration: Duration(milliseconds: 300),
      child: Align(
          alignment: Alignment.topLeft,
          child: Container(
            // margin: EdgeInsets.only(left: 10, right: 10, bottom: 2),
            decoration: BoxDecoration(
                // color: Colors.black45
                ),
            child: chewieController.isLive
                ? Text('')
                : Text(
                    chewieController.titleVideo,
                    //   child: chewieController.isLive ? Text('') : Text("Ep."+chewieController.numberEpisode.toString()+
                    // " " + chewieController.titleVideo,
                    style: TextStyle(
                      // decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 17,
                      // shadows: [
                      //   Shadow(
                      //       color: Colors.black,
                      //       blurRadius: 10.0,
                      //       offset: Offset(5.0, 5.0)),
                      //   Shadow(
                      //       color: Colors.black,
                      //       blurRadius: 10.0,
                      //       offset: Offset(5.0, 5.0))
                      // ],
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
          )),
    );
  }

  AnimatedOpacity _buildBottomBar(
    BuildContext context,
  ) {
    final iconColor = Theme.of(context).textTheme.button.color;

    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: Container(
        margin: EdgeInsets.all(5),
        height: barHeight,
        color: Colors.transparent,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          // mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            // _buildPlayPause(controller),
            chewieController.isLive
                ? Expanded(child: const Text(''))
                : _buildPosition(iconColor),
            chewieController.isLive ? const SizedBox() : _buildProgressBar(),
            // chewieController.allowMuting
            //     ? _buildMuteButton(controller)
            //     : Container(),
            // chewieController.
            chewieController.isLive == false ? _settingQuality() : Container(),
            chewieController.allowFullScreen && chewieController.isLive == false
                ? _buildExpandButton()
                : Container(),
            chewieController.allowFullScreen && chewieController.isLive
                ? rowLiveStream()
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget inliveTopBar() {
    return Container(child: _buildtitleLive(context)
        // child: Text("En vivo", style: TextStyle(color: Colors.white)),
        );
  }

  AnimatedOpacity _buildtitleLive(BuildContext context) {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: Align(
          alignment: Alignment.topLeft,
          // child: Container(
          //   color: Colors.black,
          //   child: Container(
          //       margin: EdgeInsets.all(3),
          //       child: Text(
          //         chewieController.qualityType,
          //         style: TextStyle(
          //             color: Colors.white,
          //             fontSize: 20,
          //             fontWeight: FontWeight.bold),
          //       )),
          // ),
          child: Row(
            children: <Widget>[
              Container(
                color: Colors.red,
                child: Container(
                    margin: EdgeInsets.all(3),
                    child: Text(
                      "En vivo",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    )),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  color: Colors.black,
                  child: Container(
                      margin: EdgeInsets.all(4.5),
                      child: Text(
                        chewieController.qualityType,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      )))
            ],
          )),
    );
  }

  Widget rowLiveStream() {
    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        // Container(
        //   decoration: BoxDecoration(color: Colors.red, shape: BoxShape.circle),
        //   // margin: EdgeInsets.all(3),
        //   // height: barHeight,
        //   // child: Text("."),
        //   child: Icon(
        //     Icons.radio_button_checked,
        //     color: Colors.transparent,
        //     size: barHeight / 5,
        //   ),
        // ),
        // Container(
        //   margin: EdgeInsets.all(12.0),
        //   child: Text(
        //     'En vivo',
        //     style: TextStyle(
        //         color: Colors.white,
        //         fontWeight: FontWeight.bold,
        //         // shadows: [
        //         //   Shadow(
        //         //       color: Colors.black,
        //         //       blurRadius: 10.0,
        //         //       offset: Offset(5.0, 5.0))
        //         // ],
        //         fontSize: 17),
        //   ),
        // ),
        _settingQuality(),
        _buildExpandButton(),
      ],
    );
  }

  GestureDetector _buildExpandButton() {
    return _latestValue == null ||
            _latestValue.position == null ||
            _latestValue.duration == null
        ? GestureDetector(
            onTap: () {},
            child: Container(),
          )
        : GestureDetector(
            // onTap: _onExpandCollapse,
            onTap: changeorientarion,
            // chewieController.autoOrientation
            //     ? changeorientarion
            //     : _onExpandCollapse,
            child: AnimatedOpacity(
              opacity: _hideStuff ? 0.0 : 1.0,
              duration: Duration(milliseconds: 300),
              child: Container(
                  height: barHeight,
                  margin: EdgeInsets.only(right: 12.0),
                  padding: EdgeInsets.only(
                    left: 8.0,
                    right: 8.0,
                  ),
                  child: Center(
                    child: Icon(
                      MediaQuery.of(context).orientation ==
                              Orientation.landscape
                          ? Icons.fullscreen_exit
                          : Icons.fullscreen,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                  // child: chewieController.autoOrientation
                  //     ? Center(
                  //         child: Icon(
                  //           MediaQuery.of(context).orientation == Orientation.landscape
                  //               ? Icons.fullscreen_exit
                  //               : Icons.fullscreen,
                  //         ),
                  //       )
                  // : Center(
                  //     child: Icon(
                  //       chewieController.isFullScreen
                  //           ? Icons.fullscreen_exit
                  //           : Icons.fullscreen,
                  //       color: Colors.white,
                  //     ),
                  //   ),
                  ),
            ),
          );
  }

  void changeorientarion() {
    if (fullScreen == false) {
      setState(() {
        fullScreen = true;
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);
      });
    } else if (fullScreen) {
      setState(() {
        fullScreen = false;
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      });
    }
  }

  Widget _settingQuality() {
    return GestureDetector(
      onTap: () {},
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          margin: EdgeInsets.only(right: 12.0),
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: _latestValue == null ||
                  _latestValue.position == null ||
                  _latestValue.duration == null
              ? Text('')
              : chewieController.settingQuality,
          // child: Center(
          //   child: Icon(
          //     chewieController.isFullScreen
          //         ? Icons.fullscreen_exit
          //         : Icons.fullscreen,
          //   ),
          // ),
        ),
      ),
    );
  }

  Expanded _buildHitArea() {
    return Expanded(
      child: GestureDetector(
          onTap: () {
            // if (_hideStuff) {
            //   setState(() {
            //     _hideStuff = false;
            //   });
            // } else if (_hideStuff == false) {
            //   setState(() {
            //     _hideStuff = true;
            //   });
            // }
            // if (_latestValue != null && _latestValue.isPlaying) {
            //   if (_displayTapped) {
            //     setState(() {
            //       _hideStuff = true;
            //     });
            //   } else
            //     _cancelAndRestartTimer();
            // } else {
            //   _playPause();

            //   setState(() {
            //     _hideStuff = true;
            //   });
            // }
          },
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              screenAlertTimer("back"),
              chewieController.isLive == false
                  ? GestureDetector(
                      onTap: () {
                        // _hideTimerICON();
                        _hideBack = false;
                        _backOption = Timer(const Duration(seconds: 1), () {
                          setState(() {
                            _hideBack = true;
                          });
                        });
                        _skipBack(10);
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: AnimatedOpacity(
                          opacity: _hideStuff == false ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 300),
                          child: Icon(
                            Icons.replay_10,
                            color: Colors.white,
                            size: barHeight,
                          ),
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                width: barHeight,
                child: Container(),
              ),
              Container(
                child: AnimatedOpacity(
                  opacity: _hideStuff == false ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 300),
                  child: chewieController.isLive
                      ? Container()
                      : _buildPlayPause(controller),
                ),
              ),
              SizedBox(
                width: barHeight,
                child: Container(),
              ),
              chewieController.isLive == false
                  ? GestureDetector(
                      onTap: () {
                        controller.pause();
                        _hideFoward = false;
                        _fowardOption = Timer(const Duration(seconds: 1), () {
                          setState(() {
                            controller.play();
                            _hideFoward = true;
                          });
                        });
                        _skipForward(10);
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: AnimatedOpacity(
                          opacity: _hideStuff == false ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 300),
                          child: Icon(
                            Icons.forward_10,
                            color: Colors.white,
                            size: barHeight,
                          ),
                        ),
                      ),
                    )
                  : Container(),
              screenAlertTimer("foward")
            ],
          )
          // child: Container(
          //   color: Colors.transparent,
          //   child: Center(
          //     child: AnimatedOpacity(
          //       opacity:
          //           _latestValue != null && !_latestValue.isPlaying && !_dragging
          //               ? 1.0
          //               : 0.0,
          //       duration: Duration(milliseconds: 300),
          //       child: GestureDetector(
          //         child: Container(
          //           decoration: BoxDecoration(
          //             color: Theme.of(context).dialogBackgroundColor,
          //             borderRadius: BorderRadius.circular(48.0),
          //           ),
          //           child: Padding(
          //             padding: EdgeInsets.all(12.0),
          //             child: Icon(Icons.play_arrow, size: 32.0),
          //           ),
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          ),
    );
  }

  Widget screenAlertTimer(String type) {
    if (type == "back") {
      return chewieController.isLive
          ? Container()
          : Container(
              child: AnimatedOpacity(
                opacity: _hideBack == true || _hideBack == null ? 0.0 : 1.0,
                duration: Duration(milliseconds: 300),
                child: Icon(
                  Icons.timer_10,
                  color: Colors.white,
                  size: barHeight / 2,
                ),
              ),
            );
    } else {
      return chewieController.isLive
          ? Container()
          : Container(
              child: AnimatedOpacity(
                opacity: _hideFoward == true || _hideFoward == null ? 0.0 : 1.0,
                duration: Duration(milliseconds: 300),
                child: Icon(
                  Icons.timer_10,
                  color: Colors.white,
                  size: barHeight / 2,
                ),
              ),
            );
    }
  }

  GestureDetector _buildMuteButton(
    VideoPlayerController controller,
  ) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if (_latestValue.volume == 0) {
          controller.setVolume(_latestVolume ?? 0.5);
        } else {
          _latestVolume = controller.value.volume;
          controller.setVolume(0.0);
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            child: Container(
              height: barHeight,
              padding: EdgeInsets.only(
                left: 8.0,
                right: 8.0,
              ),
              child: Icon(
                (_latestValue != null && _latestValue.volume > 0)
                    ? Icons.volume_up
                    : Icons.volume_off,
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildPlayPause(VideoPlayerController controller) {
    return _latestValue != null &&
                !_latestValue.isPlaying &&
                _latestValue.duration == null ||
            _dragging ||
            _hideBack == false ||
            _hideFoward == false ||
            _latestValue.isBuffering
        ? GestureDetector(
            child: const Center(
              child: const CircularProgressIndicator(),
            ),
          )
        : GestureDetector(
            onTap: _playPause,
            child: Container(
              height: barHeight,
              color: Colors.transparent,
              // margin: EdgeInsets.only(left: 8.0, right: 8.0),
              // padding: EdgeInsets.only(
              //   left: 12.0,
              //   right: 12.0,
              // ),
              child: Icon(
                controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
                size: barHeight,
              ),
            ),
          );
  }

  Widget _buildPosition(Color iconColor) {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;
    final duration = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration
        : Duration.zero;

    return Padding(
      padding: EdgeInsets.only(right: 24.0),
      child: _latestValue == null ||
              _latestValue.position == null ||
              _latestValue.duration == null
          ? Text(
              '- / -',
              style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
              ),
            )
          : Text(
              '${formatDuration(position)} / ${formatDuration(duration)}',
              style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
              ),
            ),
    );
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();
    // _backOption?.cancel();
    // _fowardOption?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
      _displayTapped = true;
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController.autoPlay) {
      _startHideTimer();
    }

    if (chewieController.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _skipBack(int skipNumber) {
    if (_latestValue != null ||
        _latestValue.duration != null ||
        _latestValue.position != null) {
      // int parseSkipNumber = skipNumber == 120 ? 10 : skipNumber;
      _cancelAndRestartTimer();
      final beginning = Duration(seconds: 0).inMilliseconds;
      final skip = (_latestValue.position - Duration(seconds: skipNumber))
          .inMilliseconds;
      controller.seekTo(Duration(milliseconds: math.max(skip, beginning)));
    }
  }

  void _skipForward(int skipNumber) {
    if (_latestValue != null ||
        _latestValue.duration != null ||
        _latestValue.position != null) {
      // int parseSkipNumber = skipNumber == 120 ? 10 : skipNumber;
      _cancelAndRestartTimer();
      final end = _latestValue.duration.inMilliseconds;
      final skip = (_latestValue.position + Duration(seconds: skipNumber))
          .inMilliseconds;
      controller.seekTo(Duration(milliseconds: math.min(skip, end)));
    }
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController.toggleFullScreen();
      _showAfterExpandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  void _playPause() {
    bool isFinished = _latestValue.position >= _latestValue.duration;

    setState(() {
      if (controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        controller.pause();
      } else {
        _cancelAndRestartTimer();

        if (!controller.value.initialized) {
          controller.initialize().then((_) {
            controller.play();
          });
        } else {
          if (isFinished) {
            controller.seekTo(Duration(seconds: 0));
          }
          controller.play();
        }
      }
    });
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    setState(() {
      _latestValue = controller.value;
      if (_latestValue.duration != null) {
        resetValue = _latestValue.position >= _latestValue.duration;
      }
    });
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: MaterialVideoProgressBar(
          controller,
          onDragStart: () {
            if (_latestValue != null ||
                _latestValue.position != null ||
                _latestValue.duration != null) {
              setState(() {
                _dragging = true;
              });

              _hideTimer?.cancel();
            }
          },
          onDragEnd: () {
            if (_latestValue != null ||
                _latestValue.position != null ||
                _latestValue.duration != null) {
              setState(() {
                _dragging = false;
              });

              _startHideTimer();
            }
          },
          colors: chewieController.materialProgressColors ??
              ChewieProgressColors(
                  playedColor: chewieController.playedColor == null
                      ? Theme.of(context).accentColor
                      : Color(chewieController.playedColor),
                  handleColor: chewieController.playedColor == null
                      ? Theme.of(context).accentColor
                      : Color(chewieController.handleColor),
                  bufferedColor: chewieController.playedColor == null
                      ? Theme.of(context).backgroundColor
                      : Color(chewieController.bufferedColor),
                  backgroundColor: chewieController.playedColor == null
                      ? Colors.grey[700].withOpacity(0.7)
                      : Color(chewieController.backgroundColor)),
        ),
      ),
    );
  }
}
